import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:xbrpgui/config.dart';

import 'package:xbrpgui/src/screens/login.dart';
import 'package:xbrpgui/src/screens/registration.dart';
import 'package:xbrpgui/src/screens/registration_result.dart';
import 'package:xbrpgui/src/screens/wedding.dart';
import 'package:xbrpgui/src/screens/welcome.dart';

void main() => runApp(ManagementApp());

final String WelcomeRoute = '/welcome';
final String RegistrationRoute = '/registration';
final String RegistrationResultRoute = '/result';
final String WeddingRoute = "/wedding";

class ManagementApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        canvasColor: AppStyle.PaarsWit,
        primaryColor: AppStyle.NachtPaars,
        fontFamily: 'Open Sans',
        textTheme: TextTheme(
          headline1: TextStyle(
              color: AppStyle.BijnaZwart,
            fontWeight: FontWeight.bold,
            fontSize: 18
          ),
        ).apply(
          bodyColor: AppStyle.BijnaZwart
        )
      ),
      routes: {
        '/': (context) => LoginScreen(),
        '/': (context) => WelcomeScreen(),
        WelcomeRoute: (context) => WelcomeScreen(),
        WeddingRoute: (context) => WeddingScreen(),
        RegistrationRoute: (context) => RegistrationScreen(),
        RegistrationResultRoute: (context) => RegistrationResultScreen()
      },
    );
  }
}
