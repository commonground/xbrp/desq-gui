import 'package:flutter/material.dart';
import 'package:xbrpgui/config.dart';

class RegistrationResultScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(),
      appBar: AppBar(
        title: Text("Trouwen in ${Municipality}"),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: _buildCard(context)
      )
    );
  }
}

Widget _buildCard(BuildContext context) {
  final String partner1 = Partners[0];
  final String partner2 = Partners[1];

  return Container(
    decoration: BoxDecoration(
        border: Border.all(
            width: 2,
            color: Colors.green,
        )
    ),
    child: Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.done, color: Colors.green, size: 36),
            title: Text(
                'Registratie gelukt',
                style: TextStyle(fontWeight: FontWeight.bold)
            ),
          ),
          ListTile(
            title: Text(
                'Het huwelijk tussen ${partner1} en ${partner2} is geregistreerd en het naamgebruik is aangepast',
              style: TextStyle(height: 1.5)
            )
          ),
        ],
      ),
    ),
  );
}