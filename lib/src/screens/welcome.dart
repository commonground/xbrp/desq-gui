import 'package:flutter/material.dart';
import 'package:xbrpgui/main.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(WeddingRoute);
      },
      child: Image.asset('assets/images/splash.png')
    );
  }
}
