import 'package:flutter/material.dart';
import 'package:xbrpgui/config.dart';

import '../../main.dart';

class WeddingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(),
        appBar: AppBar(
            title: Text(
                "Trouwen in ${Municipality}")
        ),
        body:
            Container(
                padding: EdgeInsets.all(20),
                child: WeddingInfo()));
  }
}

class WeddingInfo extends StatelessWidget {
  var x = Partners.map((partner) {
    int i = Partners.indexOf(partner);
    return [
      WeddingProperty(property: "Partner ${i + 1}", value: Partners[i],),
      PropertyInfoButton(title: 'Meer persoonsgegevens')
    ];
  }).expand((element) => element).toList();
  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView(
          children: [
            Text(
                "Voorgenomen huwelijk van ${Partners[0]} en ${Partners[1]}",
                style:
                TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    height: 1.5)
            ),
            WeddingProperty(property: "Datum en tijdstip verbintenis", value: "06-06-2030 om 11:00 uur"),
            WeddingProperty(property: "Locatie verbintenis", value: "Schaapskooi\nBospad 93, 3855 Heidedal"),
            WeddingProperty(property: "Trouwambtenaar", value: "Babs de Groot"),
            PropertyGroupDivider(),
            WeddingProperty(property: "Type verbintenis", value: "Huwelijk"),
            ...Partners.map((partner) {
              int i = Partners.indexOf(partner);
              return [
                    WeddingProperty(property: "Partner ${i + 1}", value: Partners[i],),
                    PropertyInfoButton(title: 'Meer persoonsgegevens')
              ];
            }).expand((element) => element).toList(),
            PropertyGroupDivider(),
            WeddingProperty(property: "Getuigen", value: Witnesses.reduce((value, element) => value + '\n' + element)),
            PropertyInfoButton(title: 'Meer gegevens getuigen'),
            PropertyGroupDivider(),
            WeddingProperty(property: "Naamkeuze partner 1", value: "Pim van Hierden-Bloem"),
            WeddingProperty(property: "Naamkeuze partner 2", value: "Jill van Hierden-Bloem"),
            PropertyGroupDivider(),
            FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40.0),
                  side: BorderSide(color: Colors.purple, width: 1)
              ),
              color: Colors.purple,
              onPressed: () {
                Navigator.of(context).pushNamed(RegistrationRoute);
              },
              child: Text(
                  "Huwelijk registreren",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                  )
              ),
            ),
            Padding(padding: EdgeInsets.all(10)),
            FlatButton(
                hoverColor: Colors.white,
                onPressed: () {},
                child: Text(
                  "Voorgenomen huwelijk annuleren",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    color: Colors.purple
                  )
                )
            ),
          ],
        )
    );
  }
}

class PropertyGroupDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Divider(
      color: Colors.grey[800],
      height: 20,
      thickness: 1,
    );
  }
}

class PropertyInfoButton extends StatelessWidget {
  final String title;

  const PropertyInfoButton({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(40.0),
          side: BorderSide(color: Colors.purple, width: 1)
      ),
      onPressed: () {},
      child: Text(
          title,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.purple
          )
      ),
    );
  }
}

class WeddingProperty extends StatelessWidget {
  final String property;
  final String value;

  const WeddingProperty({Key key, this.property, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
      title: Text(
          property,
          style: TextStyle(
              color: Colors.grey[500],
              height: 2
          )
      ),
      subtitle: Text(
          value,
          style: TextStyle(
              color: Colors.grey[800],
              height: 1.25,
              fontSize: 18
          )
      ),
    );
  }
}
