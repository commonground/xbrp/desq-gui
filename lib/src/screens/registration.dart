import 'package:flutter/material.dart';
import 'package:xbrpgui/main.dart';

import '../../config.dart';

class RegistrationScreen extends StatelessWidget {
  String bullet = "\u2022 ";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(25),
        child: ListView(
        children: [
          IconButton(
              alignment: Alignment.topRight,
              icon: Icon(Icons.clear), onPressed: null),
          Icon(Icons.favorite, color: Colors.purple, size: 60,),
          Padding(padding: EdgeInsets.all(10),),
          Text(
              "Als trouwambtenaar bevestig en registreer ik het huwelijk van:",
            style: TextStyle(height: 1.25, fontSize: 18)
          ),
          Padding(padding: EdgeInsets.all(10),),
          ...Partners.map((partner) {
            return Text(
                bullet + ' ' + partner,
              style: TextStyle(height: 1.25, fontSize: 18, fontWeight: FontWeight.bold),
            );
          }).toList(),
          Padding(padding: EdgeInsets.all(15),),
          Text("Babs de Groot\nTrouwambtenaar\nGemeente ${Municipality}",
            style: TextStyle(height: 1.25, fontSize: 18, color: Colors.grey[600])
          ),
          Padding(padding: EdgeInsets.all(20),),
          Text("Bevestig met code", style: TextStyle(fontSize: 18)),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: List<Widget>.generate(5, (i) {
                return Container(
                  width: 75,
                  height: 75,
                  child: Padding(
                      padding: EdgeInsets.only(top: 15),
                      child: Container(
                        child: TextField(
                          onChanged: (_) {
                            // Simply proceed to registration when last digit is entered
                            // This needs to be refined later
                            if (i < 4) {
                              FocusScope.of(context).nextFocus();
                            } else {
                              Navigator.of(context).pushNamed(RegistrationResultRoute);
                            }
                          },
                          obscureText: true,
                          obscuringCharacter: "*",
                          maxLength: 1,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            counterText: '',
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: Colors.purple, width: 1),
                            ),
                            contentPadding: EdgeInsets.all(10)
                          )
                        ),
                      ),
                  ),
                );
              })
            ),
          )
        ],
    ),
      ));
  }
}
