import 'dart:ui';

const String Municipality = "Heidedal";

const List<String> Partners = [
  "Pim van Hierden",
  "Jill Bloem"
];

const List<String> Witnesses = [
  "Marie Hoekstra",
  "Jan Bloem",
  "Piet van Hierden",
  "Tiny Stip"
];

class AppStyle {
  static final NachtPaars = Color(0xFF501B1D);
  static final DonkerPaars = Color(0xFF64485C);
  static final Paars = Color(0xFF84046F);
  static final LichtPaars = Color(0xFFDEC7DA);
  static final PaarsWit = Color(0xFFF0ECEF);
  static final BijnaZwart = Color(0xFF333333);
}
