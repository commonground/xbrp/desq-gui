# xbrpgui

Simple Flutter project voor het trouw scenario

## Getting Started

De applicatie kan worden gestart door middel van:

`flutter run -d chrome`

## Indeling

De screens staat in lib/src/screens.

Enkele utility widgets staan in lib/src/widgets
In de eerste versie staat hier een animated progess indicator widget die is gekopieerd van de flutter website.

De communicatie met de XBRP API is nog niet geimplementeerd in deze versie.
 